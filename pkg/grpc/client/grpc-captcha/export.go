package client

import (
	"context"
	"fmt"

	pb "gitlab.com/gtsh77-werkstatt/grpc-captcha/pkg/proto/grpc-captcha"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func Generate(ctx context.Context, cl *Client, id, otp string) error {
	var (
		ictx     context.Context
		cf       context.CancelFunc
		conn     *grpc.ClientConn
		pbClient pb.CaptchaServiceClient

		err error
	)

	if pbClient, conn, err = cl.getConn(); err != nil {
		return err
	}
	defer conn.Close()

	ictx, cf = context.WithTimeout(metadata.NewOutgoingContext(ctx, metadata.Pairs("x-api-key", cl.config.XApiKey)), cl.config.Timeout)
	defer cf()

	if _, err = pbClient.Generate(ictx, nil); err != nil {
		return fmt.Errorf("Generate: %w", err)
	}

	return nil
}

func Verify(ctx context.Context, cl *Client, id, otp string) error {
	var (
		ictx     context.Context
		cf       context.CancelFunc
		conn     *grpc.ClientConn
		pbClient pb.CaptchaServiceClient

		err error
	)

	if pbClient, conn, err = cl.getConn(); err != nil {
		return err
	}
	defer conn.Close()

	ictx, cf = context.WithTimeout(metadata.NewOutgoingContext(ctx, metadata.Pairs("x-api-key", cl.config.XApiKey)), cl.config.Timeout)
	defer cf()

	if _, err = pbClient.Verify(ictx, &pb.VerifyCaptchaRequest{
		Id:  id,
		Otp: otp,
	}); err != nil {
		return fmt.Errorf("Verify: %w", err)
	}

	return nil
}
