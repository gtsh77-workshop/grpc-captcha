package client

import (
	"crypto/tls"
	"fmt"
	"time"

	pb "gitlab.com/gtsh77-werkstatt/grpc-captcha/pkg/proto/grpc-captcha"
	"gitlab.com/gtsh77-werkstatt/grpc-captcha/pkg/tools"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type Client struct {
	config *Config
}

type TLS struct {
	Enabled    bool
	SkipVerify bool
	DomainName string
	CrtData    string
	KeyData    string
	CrtCAData  string
}

type Config struct {
	Address string
	Port    string
	XApiKey string
	Timeout time.Duration
	TLS     *TLS
}

func NewClient(config *Config) *Client {
	return &Client{
		config: config,
	}
}

func (c *Client) getConn() (pb.CaptchaServiceClient, *grpc.ClientConn, error) {
	var (
		conn    *grpc.ClientConn
		tlsConf *tls.Config

		err error
	)

	if c.config.TLS.Enabled {
		if tlsConf, err = tools.PrepareTLS(c.config.TLS.KeyData, c.config.TLS.CrtData, c.config.TLS.CrtCAData, c.config.TLS.DomainName, c.config.TLS.SkipVerify); err != nil {
			return nil, nil, fmt.Errorf("tools.PrepareTLS: %s", err)
		}

		if conn, err = grpc.NewClient(fmt.Sprint(c.config.Address, ":", c.config.Port), grpc.WithTransportCredentials(credentials.NewTLS(tlsConf))); err != nil {
			return nil, nil, fmt.Errorf("grpc.NewClient: %w", err)
		}
	} else {
		if conn, err = grpc.NewClient(fmt.Sprint(c.config.Address, ":", c.config.Port), grpc.WithTransportCredentials(insecure.NewCredentials())); err != nil {
			return nil, nil, fmt.Errorf("grpc.NewClient: %w", err)
		}
	}

	return pb.NewCaptchaServiceClient(conn), conn, nil
}
